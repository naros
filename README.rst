Naros (Quenya for 'firewall') is a Python-based daemon that assists in management of an nftables-based firewall. The
features it provides are described in following sections.

Naros should be run as an executable Python script. It requires a directory to store its state in (by default
/var/lib/naros, may be configured with --state-dir), that directory should be created before first run.

Naros can be launched in a "dry-run" mode (-n/--dry-run), in which it never modifies the firewall state.

Naros is distributed under the GNU GPL version 3 licence (see the LICENSE file).

Persisent sets
--------------
Naros can persist the state of chosen sets, so that they survive firewall restarts. The sets that will be persisted are
those that match --persistent-set-regex, by default it is those whose names start with 'set_persist_'.

The way this works is as follows:
* Naros monitors nft state using 'nft monitor'.
* When a set is added to nftables (e.g. at firewall startup, or when modifying the firewall dynamically), Naros starts
  tracking it. If it is a set that was seen previously, Naros will add all elements from its internal state to the
  nftables set (note that it will not remove any - so elements should only be removed when Naros is running).
* When set elements are added or removed in nftables, Naros updates its internal state appropriately.
* Set state is stored to persistent_sets.json.gz in the state dir, at regular intervals (see --flush-period) and at
  program exit.
* KNOWN ISSUE: elements added to set from firewall rules are not reported via 'nft monitor', so Naros currently does not
  see them. This could be worked around by periodically re-querying nft state.
* KNOWN ISSUE: old sets are never removed from state files.

Hostname sets
-------------
Using host names in firewall rules is generally problematic, because DNS resolution may not be available at firewall
startup, and the addresses may change with time. Naros addresses both of these issues with its hostname sets, which are
special sets that Naros will dynamically update to match their hostname. Hostname sets are those that match the regex
specified with --host-set-regex (default '^set_host.*__(.*)'). The regex must contain one capture group, which should
match the hostname to be resolved. Resolved hostnames are stored to host_sets.json.gz in the state dir, which allows
them to work even if DNS resolution is temporarily unavailable.

* KNOWN LIMITATION: this cannot work for actions that need a single address rather than a set, e.g. NAT targets.
* KNOWN LIMITATION: old sets are never removed from stat files.
